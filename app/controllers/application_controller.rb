class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  def last_solved
  	@last_solveds = Solved.all.order('created_at DESC')
  end
end
