class SolvedsController < ApplicationController
  before_action :set_solved, only: [:show, :edit, :update, :destroy]

  # GET /solveds
  # GET /solveds.json
  def index
    @solveds = Solved.all
  end

  # GET /solveds/1
  # GET /solveds/1.json
  def show
  end

  # GET /solveds/new
  def new
    @solved = Solved.new
  end

  # GET /solveds/1/edit
  def edit
  end

  # POST /solveds
  # POST /solveds.json
  def create
    @solved = Solved.new(solved_params)

    respond_to do |format|
      if @solved.save
        format.html { redirect_to @solved, notice: 'Solved was successfully created.' }
        format.json { render :show, status: :created, location: @solved }
      else
        format.html { render :new }
        format.json { render json: @solved.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /solveds/1
  # PATCH/PUT /solveds/1.json
  def update
    respond_to do |format|
      if @solved.update(solved_params)
        format.html { redirect_to @solved, notice: 'Solved was successfully updated.' }
        format.json { render :show, status: :ok, location: @solved }
      else
        format.html { render :edit }
        format.json { render json: @solved.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /solveds/1
  # DELETE /solveds/1.json
  def destroy
    @solved.destroy
    respond_to do |format|
      format.html { redirect_to solveds_url, notice: 'Solved was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_solved
      @solved = Solved.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def solved_params
      params.require(:solved).permit(:name, :pokemon)
    end
end
