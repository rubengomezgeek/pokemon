module ApplicationHelper
	def aTresDigitos(numero)
		numero = numero.to_s
		if numero.length == 1
			numero = '00'+numero
		elsif numero.length == 2
			numero = '0'+numero
		end
		return numero
	end
end
