json.array!(@solveds) do |solved|
  json.extract! solved, :id, :name, :pokemon
  json.url solved_url(solved, format: :json)
end
