class PokemonControl
	require 'json'
	def initialize(pokemons=nil)
		pokemonFile = File.read(Rails.root.to_s + '/public/pokemon.json')
		@pokemons=JSON.parse(pokemonFile)
		if pokemons!=nil
			@pokemons=pokemons
		end
		@types = []
		@pokemons.select{|k,v| @types << v["type"]}
		@types.uniq!
		@moves = []
		@pokemons.select{|k,v| @moves << v["moves"]}
		@moves.uniq!
		@names = []
		@pokemons.select{|k,v| @names << v["name"]}
		@names.uniq!
		@colors = []
		@pokemons.select{|k,v| @colors << v["color"]}
		@colors.uniq!
		@caracteristicas = []
		@pokemons.select{|k,v| @caracteristicas << v["caracteristica"]}
		@caracteristicas.uniq!
		@preguntas_posibles = 6
		@pokemones_restantes = @pokemons.count
		@answer = ""
		@type_or_move = ""
	end	
	def askCanEvolve()
		return "Puede evolucionar tu pokemon"
	end
	def answerCanEvolve(answer)
		if answer =~/Yes|YES|yes/
			@pokemons.delete_if{|k,v| v["evolveTo"].nil?}
		elsif answer=~/no|NO|No/
			@pokemons.delete_if{|k,v| !v["evolveTo"].nil?}
		elsif answer=~/idk|IDK|Idk/
			
		else
			#puts "Please type yes or no"
		end
	end
	def askCharacteristicQuestion()
		random_index=rand(0..@caracteristicas.length-1)
		caracteristica=@caracteristicas[random_index]
		return caracteristica
	end
	def answerCharacteristicQuestion(answer,caracteristica)
		if answer =~/Yes|YES|yes/
			@pokemons.delete_if{|k,v| v["caracteristica"] != caracteristica}
			@types.delete_if{|c| c!=caracteristica}
		elsif answer=~/no|NO|No/
			@pokemons.delete_if{|k,v| v["caracteristica"] == caracteristica}
			@types.delete_if{|c| c==caracteristica}
		elsif answer=~/idk|IDK|Idk/		
		else
		end
	end
	def askColorQuestion()
		random_index=rand(0..@colors.length-1)
		color=@colors[random_index]
		return color
	end
	def answerColorQuestion(answer,color)
		if answer =~/Yes|YES|yes/
			@pokemons.delete_if{|k,v| v["color"] != color}
			@types.delete_if{|c| c!=color}
		elsif answer=~/no|NO|No/
			@pokemons.delete_if{|k,v| v["color"] == color}
			@types.delete_if{|c| c==color}
		elsif answer=~/idk|IDK|Idk/		
		else
		end
	end
	def askTypeQuestion()
		random_index=rand(0..@types.length-1)
		type=@types[random_index]
		return type
	end
	def answerTypeQuestion(answer,type)
		if answer =~/Yes|YES|yes/
			@pokemons.delete_if{|k,v| v["type"] != type}
			@types.delete_if{|t| t!=type}
		elsif answer=~/no|NO|No/
			@pokemons.delete_if{|k,v| v["type"] == type}
			@types.delete_if{|t| t==type}
		elsif answer=~/idk|IDK|Idk/		
		else
		end
	end
	def askMoveQuestion()
		bandera = 0
		contador = 0
		while (bandera == 0) do
			random_index=rand(0..@moves.length-1)
			moves=@moves[random_index]
			random_index2 =rand(0..moves.length-1)
			move=moves[random_index2]
			@pokemons.each do |p|
				if p.first["moves"].to_s.include?move
					contador=contador+1
				end	
			end
			unless contador >= @pokemons.count
				bandera = 1
			end
		end
		return move
	end
	def anwserMoveQuestion(answer,move)
		if answer =~/Yes|YES|yes/
			@pokemons.delete_if{|k,v| !(v["moves"].include?move)}
			@moves.delete_if{|m| m!=move}
		elsif answer=~/no|NO|No/
			@pokemons.delete_if{|k,v| v["moves"].include?move}
			@moves.delete_if{|m| m==move}	
		elsif answer=~/idk|IDK|Idk/			
		else
		end
	end
	def askNameQuestion()
		bandera = 0
		contador = 0
		while (bandera == 0) do
			random_index=rand(0..@names.length-1)
			name=@names[random_index]
			@pokemons.each do |p|
				if p.first["name"]==name
					contador=contador+1
				end
			end
			unless contador >= @pokemons.count
				bandera = 1
			end
		end
		return name		
	end
	def answerNameQuestion(answer,name)
		if answer =~/Yes|YES|yes/
			@pokemons.delete_if{|k,v| !(v["name"] =~ /^#{name[0]}(\S)*/)}
			@names.delete_if{|n| n!=name}
		elsif answer=~/no|NO|No/
			@pokemons.delete_if{|k,v| v["name"] =~ /^#{name[0]}(\S)*/}
			@names.delete_if{|n| n==name}	
		elsif answer=~/idk|IDK|Idk/
			
		else
			puts "Please type yes or no"
		end
	end
	def isThereOnlyOne?(pokemons)
		if pokemons.length>=1
			return false
		else
			return pokemons.first
		end
	end
	def siguientePregunta(numero_pregunta)
		if @pokemons.count > 1
			if numero_pregunta.to_i==0
				askCanEvolve
			else
				pregunta = rand(1..@preguntas_posibles-1)
				if pregunta == 1 && @names.count < 2
					pregunta = 2
				end
				if pregunta == 2 && @moves.count < 2
					pregunta = 3
				end
				if pregunta == 3 && @types.count < 2
					pregunta = 1
				end
				case pregunta
					when 1 
						return ["La primera letra del nombre es ",askNameQuestion.to_s[0]]
					when 2 
						return ["Puede aprender ",askMoveQuestion]
					when 3 
						return ["Es de tipo ", askTypeQuestion]
					when 4
						return ["Es de color ", askColorQuestion]
					when 5
						return ["Tu pokemon es ",askCharacteristicQuestion]
				end
			end
		else
			ultimo_pokemon = obtenerPokemones.to_s.to_s.gsub(/\"|\[|\]|,/,"")
			nombre_ultimo_pokemon = ultimo_pokemon[/name=>(.*?)color/m, 1]
			return ["El pokemon es ", nombre_ultimo_pokemon]
		end
	end
	def obtenerPokemones()
		return @pokemons.to_s
	end
	def obtenerRestantes()
		return @pokemons.count
	end
end