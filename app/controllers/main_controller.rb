class MainController < ApplicationController
  def index
  	if request.post?
  		if params["restantes"].to_i>1
	  		@pokemons=eval(params["pokemones"])
	  		@pokemon=PokemonControl.new(@pokemons)
	  		answer = params["button"]
	  		opcion = params["opcion"]
	  		opciones = JSON.parse(cookies[:opciones])
		  	if answer =~ /yes/ and opcion.length > 1
		  		opciones << opcion
		  		cookies[:opciones] = JSON.generate(opciones)
		  	end
	  		numero_pregunta = params["numeroPregunta"].to_i
	  		case numero_pregunta
	  		when 0
	  			@pokemon.answerCanEvolve(answer)
	  		when 1
	  			@pokemon.answerNameQuestion(answer,opcion)
	  		when 2
	  			@pokemon.anwserMoveQuestion(answer,opcion)
	  		when 3
	  			@pokemon.answerTypeQuestion(answer,opcion)
	  		when 4
	  			@pokemon.answerColorQuestion(answer,opcion)
	  		when 5
	  			@pokemon.answerCharacteristicQuestion(answer,opcion)
	  		end
	  		@pokemons=@pokemon.obtenerPokemones
	  		@primeraPregunta = "1"
	  		banderita = 0
	  		while banderita == 0 do
	  			@siguientePregunta = @pokemon.siguientePregunta(@primeraPregunta)
	  			unless opciones =~ /#{@siguientePregunta[1]}/
	  				banderita = 1
	  			end
	  		end
	  		if @siguientePregunta[0] =~ /La primera/
		  		@numero_pregunta = "1"
		  	elsif @siguientePregunta[0] =~ /Puede aprender/
		  		@numero_pregunta = "2"
		  	elsif @siguientePregunta[0] =~ /Es de tipo/
		  		@numero_pregunta = "3"
		  	elsif @siguientePregunta[0] =~ /Puede evolucionar tu pokemon/
		  		@numero_pregunta = "0"
		  	elsif @siguientePregunta[0] =~ /Es de color/
		  		@numero_pregunta = "4"
		  	elsif @siguientePregunta[0] =~ /Tu pokemon es/
		  		@numero_pregunta = "5"
		  	end 
		  	@pregunta = @siguientePregunta.to_s.gsub(/\"|\[|\]|,/,"")
		  	@restantes = @pokemon.obtenerRestantes
	  	end
	  	if @restantes.to_i == 1
	  		s = Solved.new(name: "#{cookies[:name]}", pokemon: "ElPokemon")
	  		s.save!
	  	end
  	else
  		@pokemon=PokemonControl.new
  		@primeraPregunta = "0"
  		@siguientePregunta = @pokemon.siguientePregunta(@primeraPregunta)
  		if @siguientePregunta[0] =~ /La primera/
	  		@numero_pregunta = "1"
	  	elsif @siguientePregunta[0] =~ /Puede aprender/
	  		@numero_pregunta = "2"
	  	elsif @siguientePregunta[0] =~ /Es de tipo/
	  		@numero_pregunta = "3"
	  	elsif @siguientePregunta[0] =~ /Puede evolucionar tu pokemon/
	  		@numero_pregunta = "0"
	  	elsif @siguientePregunta[0] =~ /Es de color/
	  		@numero_pregunta = "4"
	  	elsif @siguientePregunta[0] =~ /Tu pokemon es/
	  		@numero_pregunta = "5"
	  	end 
	  	@pregunta = @siguientePregunta.to_s.gsub(/\"|\[|\]|,/,"")
	  	@restantes = @pokemon.obtenerRestantes
	  	opciones = ["evolve"]
	  	cookies[:opciones] = JSON.generate(opciones)

  	end  	 
  end
  def welcome
  	if request.post?
  		cookies[:nombre] = params["nombre"]
  		redirect_to :action => "index"
  	end	
  end
end
