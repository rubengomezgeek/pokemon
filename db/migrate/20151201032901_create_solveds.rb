class CreateSolveds < ActiveRecord::Migration
  def change
    create_table :solveds do |t|
      t.string :name
      t.string :pokemon

      t.timestamps null: false
    end
  end
end
