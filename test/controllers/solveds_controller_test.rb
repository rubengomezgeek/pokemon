require 'test_helper'

class SolvedsControllerTest < ActionController::TestCase
  setup do
    @solved = solveds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:solveds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create solved" do
    assert_difference('Solved.count') do
      post :create, solved: { name: @solved.name, pokemon: @solved.pokemon }
    end

    assert_redirected_to solved_path(assigns(:solved))
  end

  test "should show solved" do
    get :show, id: @solved
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @solved
    assert_response :success
  end

  test "should update solved" do
    patch :update, id: @solved, solved: { name: @solved.name, pokemon: @solved.pokemon }
    assert_redirected_to solved_path(assigns(:solved))
  end

  test "should destroy solved" do
    assert_difference('Solved.count', -1) do
      delete :destroy, id: @solved
    end

    assert_redirected_to solveds_path
  end
end
